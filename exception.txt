static void Main(string[] args)
        {
            var x = new int[] {1,2,3,4};

            try
            {
                test(x);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void test(int[] numbers)//wypisywanie tablicy intow
        {
            //throw new ArgumentNullException();
            if (numbers == null) throw new ArgumentNullException("numbers");
            else if (numbers.Length == 0) throw new ArgumentException("empty");
            Console.WriteLine(numbers.Average());
        }