﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    public abstract class Prism : Shape
    {
        protected Prism(double height) : base(height)
        {
        }

        public override double GetVolume()
        {
            return GetBaseArea() * Height;
        }

        protected abstract double GetBaseArea();
    }
}
