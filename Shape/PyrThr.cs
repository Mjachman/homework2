﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    public class PyrThr : Pyramid, IEquatable<PyrThr>
    {
        public double A { get; set; }

        public double B { get; set; }

        public double C { get; set; }

        public PyrThr(double height, double a, double b, double c) : base(height)//przekazywanie
        {
            this.A = new double[] { a, b, c }.Min(); 
            this.B = Math.Min(Math.Min(Math.Max(a, b), Math.Max(b, c)), Math.Max(a, c));
            this.C = new double[] { a, b, c }.Max();
    
            //Console.WriteLine($"{this.A}, {this.B}, {this.C}"); 
            if (a <= 0 || b <= 0 || c <= 0)
            {
                throw new ArgumentOutOfRangeException("Wartość niedodatnia");
            }
            if (double.IsNaN(a) || double.IsNaN(b) || double.IsNaN(c))
            {
                throw new ArgumentException("Wartość NaN");
            }
            if (A + B <= C)
            {
                throw new ArgumentException("Podane długości nie mogą stworzyć trójkąta");
            }

        }

        protected override double GetBaseArea()
        {
            return Math.Sqrt((A + B + C) * (A + B - C) * (A - B + C) * (B + C - A)) / 4.0;
        }

        public bool Equals(PyrThr other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return A.Equals(other.A) && B.Equals(other.B) && C.Equals(other.C) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PyrThr) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = A.GetHashCode();
                hashCode = (hashCode * 397) ^ B.GetHashCode();
                hashCode = (hashCode * 397) ^ C.GetHashCode();
                hashCode = (hashCode * 397) ^ Height.GetHashCode();
                return hashCode;
            }
        }
    }
}
