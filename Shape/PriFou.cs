﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
//ing Shape.interfaces;

namespace Shape
{
    public class PriFou : Prism,  IEquatable<PriFou>, IComparable<PriFou>
    {
       // private ILog log = LogManager.GetLogger(typeof(PriFou));
        //ivate IDimensionsProvider @object;

        /*private readonly double _a;//pola

        private readonly double _b;*///Bardziej czytelna wersja od proper

        public PriFou(double height, double a, double b) : base(new double[] {a,b,height}.Max())//konstruktor
        {
            this.A = new double[] { a, b, height }.Min();
            this.B = Math.Min(Math.Min(Math.Max(a, b), Math.Max(b, height)), Math.Max(a, height));
            if (a <= 0 || b <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (double.IsNaN(a) || double.IsNaN(b)) 
            {
                throw new ArgumentException();
            }
        } //konstruktor pierwszy

        //public PriFou(double height, double a, double b)
           // : this(new DummyDimensionProvider(new Dictionary<string, double> //konstruktor
            //{
            //    ["a"] = a,
            //    ["b"] = b,
            //    ["h"] = height
            //}))

//        {

//        }
//public PriFou(IDimensionsProvider @object)
//        {
//            this.@object = @object;
        //}

        //public PriFou(IDimensionsProvider dp) : base(dp.GetDimension("h"))
        //{
        //    if(!dp.CheckIfAllPositive("a", "b", "h")) throw new ArgumentException(); //dwa razy odpytuje
        //    //if(!DPExtensionMethods.CheckIfAllPositive(dp)) throw new ArgumentException(); //tradycyjne wywołanie
        //    A = dp.GetDimension("a");
        //    B = dp.GetDimension("b");
        //}//konstruktor drugi

        protected override double GetBaseArea()
        {
            return A * B;
        }

        public double A { get; }//prop
        public double B { get; }

        public bool Equals(PriFou other)
        {
           // log.Info("Equals od PriFou");
            if (ReferenceEquals(null, other)) return false;//referenceequals porownuje czy to jest ten sam obiekt
            if (ReferenceEquals(this, other)) return true;
            return A.Equals(other.A) && B.Equals(other.B) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
           // log.Info("Equals od obj");
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PriFou) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (A.GetHashCode() * 397) ^ B.GetHashCode();
            }
        }

        public int CompareTo(PriFou other)
        {
            //log.Info("CompareTo od PriFou");//ze zostala wywołana
            //log.Debug($"przyjąłem graniastosłup o podstawie prostokąta: {other?.A ?? double.NaN} {other?.B ?? double.NaN} {other?.Height ?? double.NaN}"); //?? jeżeli jest nullem, że sie wysypała
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return GetVolume().CompareTo(other.GetVolume());
        }
    }
}
