﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    public abstract class Pyramid : Shape
    {
        protected Pyramid(double height) : base(height)
        {
        }

        public override double GetVolume()
        {
            return GetBaseArea() * Height /3;
        }

        protected abstract double GetBaseArea();
    }
}
