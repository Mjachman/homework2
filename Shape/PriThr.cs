﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    class PriThr : Prism
    {
        /*private readonly double _a;
        private readonly double _b;*///pola

        public PriThr(double height, double a, double b, double c) : base(height)
        {
            this.A = new double[] { a, b, c }.Min();
            this.B = Math.Min(Math.Min(Math.Max(a, b), Math.Max(b, c)), Math.Max(a, c));
            this.C = new double[] { a, b, c }.Max();
            if (a <= 0 || b <= 0  || c<=0)
            {
                throw new ArgumentOutOfRangeException("Wartość niedodatnia");
            }
            if (double.IsNaN(a) || double.IsNaN(b) || double.IsNaN(c))
            {
                throw new ArgumentOutOfRangeException("Wartość NaN");
            }
            if (A + B <= C)
            {
                throw new ArgumentException("Podane długości nie mogą stworzyć trójkąta");
            }
        }

        protected override double GetBaseArea()
        {
            return Math.Sqrt((A + B + C) * (A + B - C) * (A - B + C) * (B + C - A)) / 4.0;
        }

        public double A { get; }
        public double B { get; }
        public double C { get; }
    }
}
