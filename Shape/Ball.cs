﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Shape
{
    public class Ball : Shape /*IComparable<Ball>*/
    {
        //public int CompareTo(Ball other)
        //{
        //    if (ReferenceEquals(this, other)) return 0;
        //    if (ReferenceEquals(null, other)) return 1;
        //    return Radius.CompareTo(other.Radius);
        //}

        public override string ToString()//metoda
        {
            return $"Kula o promieniu: {Radius}";
        }

        public Ball(double radius) : base(2*radius)
        {
            if (radius <= 0 || double.IsNaN(radius)) throw new ArgumentOutOfRangeException(nameof(radius), radius, "Promień kuli musi być dodatni");
            this.Radius = radius;
        }

        public double Radius { get; }

        public override double GetVolume()
        {
            return 4 * Math.PI * Math.Pow(Radius, 3) / 3;
        }
    }
}
