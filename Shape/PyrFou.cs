﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
//ing Shape.interfaces;

namespace Shape
{
    public class PyrFou : Pyramid, IEquatable<PyrFou>
    {
        
        public PyrFou(double height, double a, double b) : base(height)//konstruktor
        {
            this.A = new double[] {a, b}.Max(); ; //to list
            this.B = new double[] {a, b}.Min();
            if (a <= 0 || b <= 0)
            {
                throw new ArgumentOutOfRangeException("");
            }
            if (double.IsNaN(a) || double.IsNaN(b))
            {
                throw new ArgumentOutOfRangeException(" NaN");
            }
        } 

        protected override double GetBaseArea()
        {
            return A * B;
        }

        public double A { get; }
        public double B { get; }

        public bool Equals(PyrFou other)
        {
            
            if (ReferenceEquals(null, other)) return false;//referenceequals porownuje czy to jest ten sam obiekt
            if (ReferenceEquals(this, other)) return true;
            return A.Equals(other.A) && B.Equals(other.B) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
            
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PyrFou)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (A.GetHashCode() * 397) ^ B.GetHashCode();
            }
        }
    }
}
