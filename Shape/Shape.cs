﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    public abstract class Shape
    {
        protected  double _height;

        protected Shape(double height)
        {
            if (height <= 0)
            {
                throw new ArgumentOutOfRangeException("height", "wysokosć ujemna");
            }
            if (double.IsNaN(height))
            {
                throw new ArgumentException(/*nameof(height), */"wysokość NaN");
            }
            _height = height;
        }

        public abstract double GetVolume();

        public int CompareTo(Shape other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return GetVolume().CompareTo(other.GetVolume());
        }

        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }
    }
}
