﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using NUnit.Framework.Internal;
using Shape;

namespace NUnit.Tests1
{
    [TestFixture]
    public class PyrThrTest1
    { [Test]
        public void CompareTo_Null_PositiveResult()
        {
            var ostroslupA = new PyrThr(3, 2, 5, 4);
            var result = ostroslupA.CompareTo(null);
            Assert.That(result, Is.Positive);
        }

        [Test]
        public void CompareTo_This_ZeroResult()
        {
            var ostroslupA = new PyrThr(3, 2, 5, 4);
            var result = ostroslupA.CompareTo(ostroslupA);
            Assert.That(result, Is.Zero);
        }

        [Test]
        public void Volume_ProperDimensions_ResultOk([Values(1, 2, 3)]double h, [Values(3,4,5)]double a, [Values(3,4,5)]double b, [Values(3,4,5)]double c) //test pozytywny
        {
            var ostroslup = new PyrThr(h, a, b, c); //Am

            var volume = ostroslup.GetVolume(); //Act

            Assert.That(volume, Is.EqualTo((Math.Sqrt((a + b + c) * (a + b - c) * (a - b + c) * (b + c - a)) / 4.0)*h/3).Within(0.000000000001)); //Assert
        }

        [Test]
        public void CompareTo_Identical_ZeroResult() //test pozytywny
        {
            var ostroslup = new PyrThr(3, 5, 6, 3); //Am

            var result = ostroslup.CompareTo(new PyrThr(3, 5, 6, 3)); //Act

            Assert.That(result, Is.Zero); //Asser
        }

        [Test]
        public void CompareTo_Smaller_PositiveResult() //test pozytywny
        {
            var ostroslup = new PyrThr(3, 5, 6, 3); //Am

            var result = ostroslup.CompareTo(new PyrThr(1, 5, 6, 3)); //Act

            Assert.That(result, Is.Positive); //Asser

        }
        [Test]
        public void CompareTo_Bigger_NegativeResult() //test pozytywny
        {
            var ostroslup = new PyrThr(3, 5, 6, 3); //Am

            var result = ostroslup.CompareTo(new PyrThr(12, 5, 6, 3)); //Act

            Assert.That(result, Is.Negative); //Assert
        }

        [Test]
        public void EqualTo_Identical_AEqualsToB() //test pozytywny
        {
            var ostroslupA = new PyrThr(3, 5, 6, 3); //Am

            var ostroslupB = (new PyrThr(3, 6, 3, 5)); //Act

            Assert.That(ostroslupA.Equals(ostroslupB)); //Assert
        }
      

        [Test]
        public void EqualTo_Different_AIsNotEqualToB() //test pozytywny
        {
            var ostroslupA = new PyrThr(3, 5, 6, 3); //Am

            var ostroslupB = (new PyrThr(3, 7, 3, 6)); //Act

            Assert.AreNotEqual(ostroslupA, ostroslupB); //Assert
        }
        //[Test]
        //[Category("Negative")]
        //public void Constructor_NegativeHeight_Exception() //test negatywny
        //{
        //    Assert.Throws<ArgumentOutOfRangeException>(() => new Shape.PyrThr(-7, 6, 3, 6)); //częścia Act
        //}
        //[Test]
        //[Category("Negative")]
        //public void Constructor_ZeroValue_Exception() //test negatywny
        //{
        //    Assert.Throws<ArgumentOutOfRangeException>(() => new Shape.PyrThr(0, 6, 3, 6)); //częścia Act
        //}
        [Test]
        public void Constructor_NotPositiveA_Exception([Values(1,2,3)]double h, [Values(0,-1)]double a, [Values(1,2,3)]double b, [Values(1,2,3)]double c) //test pozytywny
        {

            Assert.Throws<ArgumentOutOfRangeException>(() => new Shape.PyrThr(h, a, b, c)); //Asser
        }
        [Test]
        public void Constructor_NotPositiveB_Exception([Values(1, 2, 3)]double h, [Values(1,2,3)]double a, [Values(0,-1)]double b, [Values(1,2,3)]double c) //test pozytywny
        {

            Assert.Throws<ArgumentOutOfRangeException>(() => new Shape.PyrThr(h, a, b, c)); //Asser
        }
        [Test]
        public void Constructor_NotPositiveC_Exception([Values(1, 2, 3)]double h, [Values(3)]double a, [Values(1,2,3)]double b, [Values(0,-1,-2)]double c) //test pozytywny
        {

            Assert.Throws<ArgumentOutOfRangeException>(() => new Shape.PyrThr(h, a, b, c)); //Asser
        }
        [Test]
        public void Constructor_NotPositiveHeight_Exception([Values(0,-1,-2)]double h, [Values(1,2,3)]double a, [Values(1,2,3)]double b, [Values(1,2,3)]double c) //test pozytywny
        {

            Assert.Throws<ArgumentOutOfRangeException>(() => new Shape.PyrThr(h, a, b, c)); //Asser
        }
        [Test]
        public void Constructor_WrongDimensions_Exception([Values(1,2,3)]double h, [Values(22,55,66)]double a, [Values(1, 2, 3)]double b, [Values(1, 2, 3)]double c)
        {
            Assert.Throws<ArgumentException>(() => new PyrThr(h, a, b, c));
        }
        [Test]
        public void Constructor_HeightNaN_Exception([Values(double.NaN)]double h, [Values(2, 3, 4)]double a, [Values(2, 3, 3)]double b, [Values(2, 3, 3)]double c)
        {
            Assert.Throws<ArgumentException>(() => new PyrThr(h, a, b, c));
        }
        [Test]
        public void Constructor_ANaN_Exception([Values(1, 2, 3)]double h, [Values(double.NaN)]double a, [Values(double.NaN)]double b, [Values(double.NaN)]double c)
        {
            Assert.Throws<ArgumentException>(() => new PyrThr(h, a, b, c));
        }
        [Test]
        public void Constructor_BNaN_Exception([Values(1,2,3)]double h, [Values(2, 3, 4)]double a, [Values(double.NaN)]double b, [Values(2, 3, 3)]double c)
        {
            Assert.Throws<ArgumentException>(() => new PyrThr(h, a, b, c));
        }
        [Test]
        public void Constructor_CNaN_Exception([Values(1, 2, 3)]double h, [Values(2, 3, 4)]double a, [Values(double.NaN)]double b, [Values(double.NaN)]double c)
        {
            Assert.Throws<ArgumentException>(() => new PyrThr(h, a, b, c));
        }

    }
}
