﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shape;
using Assert = NUnit.Framework.Assert;

namespace NUnit.Tests1
{
    [TestFixture]
    public class PriFouTest
    {
        /*[TestCase(2, 6, 6)]*/
        [Test]
        public void Volume_ProperDimensions_ResultOk([Values(1,2,3)]double h, [Values(1,2,3)]double a, [Values(1, 2, 3)/*Range(0.1, 50.0, 0.1) wprowadzamy zakresy*/]double b) //test pozytywny
        {
            var gran = new Shape.PriFou(h, a, b); //Am

            var volume = gran.GetVolume(); //Act

            Assert.That(volume, Is.EqualTo(a*b*h).Within(0.00001)); //Asser
        }

        [Test]
        [Category("Negative")]
        /*[Framework.Ignore("to bedzie praca domowa")]*/
        public void Constructor_NegativeValue_Exception() //test negatywny
        {
            Assert.Throws<ArgumentOutOfRangeException>( () => new Shape.PriFou(-1, 6, 6));//częścia Act
        }

        /*[OneTimeSetUp]//wywołuje się przed testem/testami które uruchamiamy
        public void Prepare()
        {
            MessageBox.Show("Prepare");
        }*/

        [Test]
        public void GetBaseArea_ValuesData_AreaOk() //Co_IN_Out
        {
            var graniastoslup = new PriFou(15, 2, 3);
            var po = new PrivateObject(graniastoslup);

            //double? x = po.Invoke("GetBaseArea") as double?; Act, zwróci to co jest w getbase, rzutujemy przez as i jeżeli sie nie uda to da nam null
            double? x = (double?) po.Invoke("GetBaseArea"); // rzutowanie na doubla

            Assert.That(x, Is.EqualTo(6).Within(0.00001));
        }

        //[Test]
        //public void DpConstructor_ValidData_DiemensionsOk()
        //{
        //    var dpMoq = new Mock<IDimensionsProvider>(MockBehavior.Strict);//Mock
            
        //    dpMoq.Setup(t => t.GetDimension("a")).Returns(5);
        //    dpMoq.Setup(t => t.GetDimension("b")).Returns(4);//przypisujemy w mocku wartość
        //    dpMoq.Setup(t => t.GetDimension("h")).Returns(10);

        //    var x = new PriFou(dpMoq.Object); //Act

        //    Assert.That(x.A, Is.EqualTo(5));
        //    Assert.That(x.B, Is.EqualTo(4));
        //    Assert.That(x.Height, Is.EqualTo(10));
        //}
    }
}
